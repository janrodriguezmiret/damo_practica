package com.example.practica;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import static com.example.practica.AppDbContract.*;

public class AppDbManager {

    private AppDbHelper appDbHelper;
    private SQLiteDatabase database;

    public AppDbManager(Context context) {
        appDbHelper = new AppDbHelper(context);
        database = appDbHelper.getWritableDatabase();
    }

    /**
     * Insereix un nou estuaidant
     *
     * @param num_expedient
     * @param nom
     * @return
     */
    public void insertEstudiant(String num_expedient, String nom, List<String> assignatures) {

        Log.i("Numero d'assignatures", Integer.toString(assignatures.size()));

        ContentValues contentEstudiant = new ContentValues();
        contentEstudiant.put(Estudiants.COLUMNA_NUM_EXPEDIENT, num_expedient);
        contentEstudiant.put(Estudiants.COLUMNA_NOM, nom);
        database.insertOrThrow(Estudiants.NOM_TAULA, null, contentEstudiant);

        //L'afegim a totes les assignatures
        for (int i = 0; i < assignatures.size(); i++) {
            ContentValues contentValue = new ContentValues();
            contentValue.put(Estudiants_Assignatures.COLUMNA_NUM_EXPEDIENT, num_expedient);
            contentValue.put(Estudiants_Assignatures.COLUMNA_ASSIGNATURA, assignatures.get(i));
            database.insertOrThrow(Estudiants_Assignatures.NOM_TAULA, null, contentValue);
        }


    }


    /**
     * Insereix una nova assistència
     *
     * @param num_expedient
     * @param dia
     * @param assignatura
     * @param observacio
     * @param assistencia
     * @return
     */
    public long insertAssistencia(String num_expedient, String dia, String assignatura, String observacio, int assistencia) {
        dia = canviarFormatData(dia);
        ContentValues contentValue = new ContentValues();
        contentValue.put(Assistencies.COLUMNA_NUM_EXPEDIENT, num_expedient);
        contentValue.put(Assistencies.COLUMNA_DIA, dia);
        contentValue.put(Assistencies.COLUMNA_ASSIGNATURA, assignatura);
        contentValue.put(Assistencies.COLUMNA_OBSERVACIO, observacio);
        contentValue.put(Assistencies.COLUMNA_ASSISTENCIA, assistencia);
        return database.insertOrThrow(Assistencies.NOM_TAULA, null, contentValue);
    }

    /**
     * Actualitza una assistència
     *
     * @param num_expedient
     * @param dia
     * @param assignatura
     * @param observacio
     * @param assistencia
     * @return
     */
    public long updateAssistencia(String num_expedient, String dia, String assignatura, String observacio, int assistencia) {
        dia = canviarFormatData(dia);
        ContentValues contentValue = new ContentValues();
        contentValue.put(Assistencies.COLUMNA_DIA, dia);
        contentValue.put(Assistencies.COLUMNA_NUM_EXPEDIENT, num_expedient);
        contentValue.put(Assistencies.COLUMNA_ASSIGNATURA, assignatura);
        contentValue.put(Assistencies.COLUMNA_OBSERVACIO, observacio);
        contentValue.put(Assistencies.COLUMNA_ASSISTENCIA, assistencia);
        String whereArgs[] = {num_expedient, dia, assignatura};
        String whereClause = Assistencies.COLUMNA_NUM_EXPEDIENT + " = ? AND " + Assistencies.COLUMNA_DIA + "= ? AND " + Assistencies.COLUMNA_ASSIGNATURA + " = ?";
        return database.update(Assistencies.NOM_TAULA, contentValue, whereClause, whereArgs);
    }


    /**
     * Obté tots els estudiants donats d'alta
     *
     * @return
     */
    public Cursor getEstudiants() {
        return database.query(Estudiants_Assignatures.NOM_TAULA, null, null, null, null, null, null);
    }

    public Cursor getEstudiantsAssignatura(String assignatura) {
        String selectionArgs[] = {assignatura};
        String query = "SELECT " +
                "a." + Estudiants_Assignatures.COLUMNA_NUM_EXPEDIENT + ", " +
                "e." + Estudiants.COLUMNA_NOM +
                " FROM " + Estudiants_Assignatures.NOM_TAULA + " a INNER JOIN " + Estudiants.NOM_TAULA + " e" +
                " ON a." + Estudiants_Assignatures.COLUMNA_NUM_EXPEDIENT + " = e." + Estudiants.COLUMNA_NUM_EXPEDIENT +
                " WHERE " + "a." + Estudiants_Assignatures.COLUMNA_ASSIGNATURA + "=?";

        return database.rawQuery(query, selectionArgs);


    }


    /*public Cursor getAssignatures(){
        return database.query(Estudiants_Assignatures.NOM_TAULA, null, null, null, null, null, null);

    }*/

    /**
     * Obté totes les assistències d'una assignatura per un dia en concret
     *
     * @param dia
     * @param assignatura
     * @return
     */
    public Cursor getAssistenciesDia(String dia, String assignatura) {
        dia = canviarFormatData(dia);
        String selectionArgs[] = {dia, assignatura};
        String query = "SELECT " +
                "a." + Assistencies.COLUMNA_NUM_EXPEDIENT + ", " +
                "a." + Assistencies.COLUMNA_DIA + ", " +
                "a." + Assistencies.COLUMNA_ASSIGNATURA + ", " +
                "e." + Estudiants.COLUMNA_NOM + ", " +
                "a." + Assistencies.COLUMNA_ASSISTENCIA + ", " +
                "a." + Assistencies.COLUMNA_OBSERVACIO +
                " FROM " + Assistencies.NOM_TAULA + " a INNER JOIN " + Estudiants.NOM_TAULA + " e" +
                " ON a." + Assistencies.COLUMNA_NUM_EXPEDIENT + " = e." + Estudiants.COLUMNA_NUM_EXPEDIENT +
                " WHERE " + "a." + Assistencies.COLUMNA_DIA + "=? AND a." + Assistencies.COLUMNA_ASSIGNATURA + "=?";
        return database.rawQuery(query, selectionArgs);
    }

    /**
     * Obté totes les assistències d'un estudiant en una assignatura
     *
     * @param estudiant
     * @param assignatura
     * @return
     */
    public Cursor getAssistenciesEstudiant(Estudiant estudiant, String assignatura) {
        String selectionArgs[] = {estudiant.getNum_expedient(), assignatura};
        String query = "SELECT " +
                "a." + Assistencies.COLUMNA_NUM_EXPEDIENT + ", " +
                "a." + Assistencies.COLUMNA_DIA + ", " +
                "a." + Assistencies.COLUMNA_ASSISTENCIA + ", " +
                "a." + Assistencies.COLUMNA_OBSERVACIO + ", " +
                "e." + Estudiants.COLUMNA_NOM +
                " FROM " + Assistencies.NOM_TAULA + " a INNER JOIN " + Estudiants_Assignatures.NOM_TAULA + " ea" +
                " ON a." + Assistencies.COLUMNA_NUM_EXPEDIENT + " = ea." + Estudiants_Assignatures.COLUMNA_NUM_EXPEDIENT +
                " AND a." + Assistencies.COLUMNA_ASSIGNATURA + " =  ea." + Estudiants_Assignatures.COLUMNA_ASSIGNATURA +
                " INNER JOIN " + Estudiants.NOM_TAULA + " e ON a." + Assistencies.COLUMNA_NUM_EXPEDIENT + " = e." + Estudiants.COLUMNA_NUM_EXPEDIENT +
                " WHERE " + "a." + Assistencies.COLUMNA_NUM_EXPEDIENT + "=? AND a." + Assistencies.COLUMNA_ASSIGNATURA + "=? ORDER BY " +
                Assistencies.COLUMNA_DIA + " DESC";
        return database.rawQuery(query, selectionArgs);
    }

    public String canviarFormatData(String data) {
        String dataParts[] = data.split("/");

        String part0 = dataParts[0];
        String part1 = dataParts[1];
        String part2 = dataParts[2];

        if (part0.length() == 1) {
            part0 = "0" + part0;
        }

        if (part1.length() == 1) {
            part1 = "0" + part1;
        }

        if (part2.length() == 1) {
            part2 = "0" + part2;
        }


        return part2 + "/" + part1 + "/" + part0;
    }


}
