package com.example.practica;

import android.app.Activity;
//import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import static com.example.practica.AppDbContract.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class PassarLlistaActivity extends Activity {

    private ListView listView;
    private PassarLlistaAdapter adapter;
    private ConjuntAssistencies conjuntAssistencies;
    private AppDbManager appDbManager;
    private TextView dataText;
    private ImageButton botoCalendari;
    private String assignatura;
    private DatePickerDialog calendari;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        this.assignatura = extras.getString("assignatura");
        Log.i("Assignatura:", assignatura);
        inicialitza(savedInstanceState);
    }


    private void inicialitza(Bundle savedInstanceState) {
        listView = findViewById(R.id.listview);
        dataText = findViewById(R.id.dataText);

        appDbManager = new AppDbManager(this);

        TextView titol = findViewById(R.id.titol);
        titol.setText(assignatura);
        String dataSeleccionada;
        if ((savedInstanceState != null) && (savedInstanceState.getSerializable("diaSeleccionat") != null)) {
            dataSeleccionada = (String) savedInstanceState.getSerializable("diaSeleccionat");

        } else {
            dataSeleccionada = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        }

        //Passar el dia seleccionat
        carregaDia(dataSeleccionada);

    }


    public void carregaDia(String dia) {
        //Rebre el dia seleccionat
        dataText = findViewById(R.id.dataText);
        dataText.setText(dia);

        botoCalendari = findViewById(R.id.botoCalendari);
        botoCalendari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarCalendari();
            }
        });
        conjuntAssistencies = new ConjuntAssistencies(appDbManager, dia, assignatura);
        if (conjuntAssistencies.size() == 0) {
            novaLlista();
        }
        adapter = new PassarLlistaAdapter(this, conjuntAssistencies);
        listView.setAdapter(adapter);
        listView.setClickable(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView nomEstudiant = view.findViewById(R.id.nom);
                String nom = nomEstudiant.getText().toString();
                Log.i("Nom estudiant: ", nom);

            }
        });
    }


    public void mostrarCalendari() {

        String diaSeleccionat = dataText.getText().toString();
        String dataParts[] = diaSeleccionat.split("/");
        String diaS = dataParts[0];
        String mesS = dataParts[1];
        String anyS = dataParts[2];

        int dia = Integer.parseInt(diaS);
        int mes = Integer.parseInt(mesS) - 1;
        int any = Integer.parseInt(anyS);

        calendari = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                carregaDia(diaToString(year, monthOfYear + 1, dayOfMonth));
            }
        }, any, mes, dia);

        calendari.getDatePicker().setMaxDate(System.currentTimeMillis());
        calendari.setButton(DatePickerDialog.BUTTON_POSITIVE, "Acceptar", calendari);
        calendari.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel·lar", calendari);
        calendari.show();
    }


    public void openHelpActivity() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    public void openAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void openPerfilEstudiantActivity(Estudiant estudiant, String assignatura) {
        Intent intent = new Intent(this, PerfilEstudiantActivity.class);
        Bundle extras = new Bundle();
        extras.putString("NomEstudiant", estudiant.getNom());
        extras.putString("NumExpedient", estudiant.getNum_expedient());
        extras.putString("assignatura", assignatura);
        intent.putExtras(extras);
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putSerializable("diaSeleccionat", dataText.getText().toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.v(TAG, "Dins del onRestore");
        String dataSeleccionada = (String) savedInstanceState.getSerializable("diaSeleccionat");
        dataText.setText(dataSeleccionada);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_help:
                openHelpActivity();
                return true;
            case R.id.action_about:
                openAboutActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void actualitzaAssistencia(Assistencia assistencia) {
        adapter.actualitzaAssistencia(assistencia);
        listView.invalidate();
        listView.invalidateViews();
    }

    public String diaToString(int year, int month, int dayOfMonth) {
        return Integer.toString(dayOfMonth) + "/" + Integer.toString(month) + "/" + Integer.toString(year);
    }

    /**
     * Funció que utilitza el conjunt d'estudiants per informar a la Activity que el conjunt d'assistències és buit,
     * per si se'n vol crear un
     *
     * @return
     */
    public void novaLlista() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.nova_llista_message);
        builder.setTitle(R.string.nova_llista_title);
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.inicialitzaDia(dataText.getText().toString());
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
