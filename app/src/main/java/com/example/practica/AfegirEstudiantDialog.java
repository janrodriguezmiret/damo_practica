package com.example.practica;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.media.Image;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AfegirEstudiantDialog extends Dialog {

    private Map<String, Integer> assignatures;

    public AfegirEstudiantDialog(final MenuAssignaturesActivity menuAssignaturesActivity) {
        super(menuAssignaturesActivity);
        setContentView(R.layout.afegiralumne);
        inicialitza(menuAssignaturesActivity);
    }

    /**
     * Amb una assignatura seleccionada per defecte
     *
     * @param menuAssignaturesActivity
     * @param assignatura
     */
    public AfegirEstudiantDialog(final MenuAssignaturesActivity menuAssignaturesActivity, String assignatura) {
        super(menuAssignaturesActivity);
        setContentView(R.layout.afegiralumne);
        inicialitza(menuAssignaturesActivity);
        CheckBox checkBox = findViewById(assignatures.get(assignatura));
        checkBox.setChecked(true);
    }

    private void inicialitza(final MenuAssignaturesActivity menuAssignaturesActivity) {
        ImageButton buttonAcceptar = findViewById(R.id.botoAcceptar);
        ImageButton buttonCancelar = findViewById(R.id.botoCancelar);

        buttonAcceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText campNumExpedient = findViewById(R.id.campNumExpedient);
                String num_expedient = campNumExpedient.getText().toString();
                EditText campNomEstudiant = findViewById(R.id.campNomEstudiant);
                String nom = campNomEstudiant.getText().toString();

                if (num_expedient.isEmpty()) {
                    mostrarErrorDialog(menuAssignaturesActivity, R.string.error_num_expedient_buit);
                } else if (nom.isEmpty()) {
                    mostrarErrorDialog(menuAssignaturesActivity, R.string.error_nom_buit);
                } else {
                    Estudiant estudiant = new Estudiant(num_expedient, nom);
                    List<String> assignatures = getAssignatures();
                    if (assignatures.isEmpty()) {
                        mostrarErrorDialog(menuAssignaturesActivity, R.string.no_assignatures_message);
                    } else {
                        try {
                            menuAssignaturesActivity.afegeixEstudiant(estudiant, assignatures);
                            tancar();
                        } catch (Exception e){
                            mostrarErrorDialog(menuAssignaturesActivity, R.string.error_num_expedient_repetit);
                        }
                    }
                }
            }
        });
        buttonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tancar();
            }
        });
        // Inicialitzem
        assignatures = new HashMap<String, Integer>();
        assignatures.put("Català", R.id.catalaBox);
        assignatures.put("Castellà", R.id.castellaBox);
        assignatures.put("Anglès", R.id.anglesBox);
        assignatures.put("Matemàtiques", R.id.matesBox);
        assignatures.put("Tecnologia", R.id.tecnologiaBox);
        assignatures.put("Ciències naturals", R.id.naturalsBox);
        assignatures.put("Plàstica", R.id.plasticaBox);
        assignatures.put("Història", R.id.historiaBox);
    }

    private void mostrarErrorDialog(MenuAssignaturesActivity menuAssignaturesActivity, int errorMessageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(menuAssignaturesActivity);
        builder.setMessage(errorMessageId);
        builder.setPositiveButton(R.string.acord, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public List<String> getAssignatures() {
        List<String> assignatures = new ArrayList<>();
        CheckBox catalabox = findViewById(R.id.catalaBox);
        if (catalabox.isChecked()) {
            assignatures.add("Català");

        }
        CheckBox castellabox = findViewById(R.id.castellaBox);
        if (castellabox.isChecked()) {
            assignatures.add("Castellà");
        }

        CheckBox anglesbox = findViewById(R.id.anglesBox);
        if (anglesbox.isChecked()) {
            assignatures.add("Anglès");

        }

        CheckBox matesbox = findViewById(R.id.matesBox);
        if (matesbox.isChecked()) {
            assignatures.add("Matemàtiques");

        }

        CheckBox tecnobox = findViewById(R.id.tecnologiaBox);
        if (tecnobox.isChecked()) {
            assignatures.add("Tecnologia");

        }

        CheckBox naturalsbox = findViewById(R.id.naturalsBox);
        if (naturalsbox.isChecked()) {
            assignatures.add("Ciències naturals");

        }

        CheckBox plasticabox = findViewById(R.id.plasticaBox);
        if (plasticabox.isChecked()) {
            assignatures.add("Plàstica");

        }

        CheckBox historiabox = findViewById(R.id.historiaBox);
        if (historiabox.isChecked()) {
            assignatures.add("Història");

        }

        return assignatures;


    }


    public void mostrar() {
        super.show();
    }


    public void tancar() {
        super.dismiss();
    }

}
