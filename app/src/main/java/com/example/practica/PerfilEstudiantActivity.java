package com.example.practica;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

public class PerfilEstudiantActivity extends Activity {
    private ListView listView;
    private AssistenciaEstudiantAdapter adapter;
    private ConjuntAssistencies conjuntAssistencies;
    private Estudiant estudiant;
    private AppDbManager appDbManager;
    private TextView nomEstudiant;
    private TextView numExpedient;
    private String assignatura;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_estudiant);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String nomEstudiant = extras.getString("NomEstudiant");
        String numExpedient = extras.getString("NumExpedient");
        assignatura = extras.getString("assignatura");
        estudiant = new Estudiant(numExpedient, nomEstudiant);
        inicialitza();
    }



    public void inicialitza(){
        listView = findViewById(R.id.assistenciesEstudiant);
        nomEstudiant = findViewById(R.id.nomEstudiantTitol);
        nomEstudiant.setText(estudiant.getNom());
        numExpedient = findViewById(R.id.numExpedient);
        numExpedient.setText(estudiant.getNum_expedient());
        appDbManager = new AppDbManager(this);
        conjuntAssistencies = new ConjuntAssistencies(appDbManager, estudiant, assignatura);
        adapter = new AssistenciaEstudiantAdapter(this, conjuntAssistencies);
        listView.setAdapter(adapter);


    }

    public void openHelpActivity() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    public void openAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_help:
                openHelpActivity();
                return true;
            case R.id.action_about:
                openAboutActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
