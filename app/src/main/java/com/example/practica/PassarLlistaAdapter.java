package com.example.practica;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

public class PassarLlistaAdapter extends BaseAdapter {

    private PassarLlistaActivity passarLlistaActivity;
    private ConjuntAssistencies conjuntAssistencies;

    public PassarLlistaAdapter(PassarLlistaActivity passarLlistaActivity, ConjuntAssistencies conjuntAssistencies) {
        this.passarLlistaActivity = passarLlistaActivity;
        this.conjuntAssistencies = conjuntAssistencies;
    }

    @Override
    public int getCount() {
        return conjuntAssistencies.size();
    }

    @Override
    public Object getItem(int position) {
        return conjuntAssistencies.getAssistencia(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Retorna la vista a mostrar per l'element en la posició position
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Infla el Layout de cada Estudiant
        // TODO: fer servir ViewHolder, tal com indica el Android Studio
        convertView = LayoutInflater.from(passarLlistaActivity).inflate(R.layout.alumne_list, parent, false);
        // Obté l'estudiant
        final Assistencia assistencia = conjuntAssistencies.getAssistencia(position);
        Log.i("PASSARLLISTAADAPTER", "una assistencia de l'estudiant: " + assistencia.getEstudiant());
        final Observacio observacio = new Observacio(passarLlistaActivity, assistencia);
        final ImageButton imageButton = convertView.findViewById(R.id.mostrarDialog);
        Log.i("PASSARLLISTAADAPTER", "Observació: " + assistencia.getObservacio());
        // Què mostrar si se li va posar una observació?
        if (!assistencia.getObservacio().isEmpty()){
            imageButton.setBackgroundColor(Color.rgb(80,120,200));
        }
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                observacio.mostrar();
                notifyDataSetChanged();
            }
        });

        // Indica el seu nom
        Button nom = convertView.findViewById(R.id.nom);
        nom.setText(assistencia.getEstudiant().getNom());

        nom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passarLlistaActivity.openPerfilEstudiantActivity(assistencia.getEstudiant(),conjuntAssistencies.getAssignatura());
            }
        });

        // La posició seleccionada de l'spinner
        Spinner spinner = convertView.findViewById(R.id.spinnerAssistencia);
        spinner.setSelection(assistencia.getAssistencia(), false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //ENTRA EN BUCLE, NO SABEM PER QUE
                assistencia.setAssistencia(position);
                actualitzaAssistencia(assistencia);
                Log.i("Adapter", assistencia.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return convertView;
    }

    /**
     * Creem les assistències per aquell dia
     * @param dia
     */
    public void inicialitzaDia(String dia){
        conjuntAssistencies.inicialitzaDia(dia);
        notifyDataSetChanged();
    }

    public void afegeixAssistencia(Assistencia assistencia) {
        conjuntAssistencies.afegeixAssistencia(assistencia);
        notifyDataSetChanged();
    }

    public void actualitzaAssistencia(Assistencia assistencia) {
        conjuntAssistencies.actualitzaAssistencia(assistencia);
        notifyDataSetChanged();
    }
}
