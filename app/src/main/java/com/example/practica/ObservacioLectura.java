package com.example.practica;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class ObservacioLectura extends Dialog {
    private String observacio;
    private TextView textView;

    // public int estat // si volem fer-ho amb estat

    public ObservacioLectura(final Context context, final String observacio) {
        super(context);
        this.setContentView(R.layout.observacio_lectura);
        this.observacio = observacio;
        textView = findViewById(R.id.observacioID);
        ImageButton botoTornar = findViewById(R.id.tornarButtonID);
        botoTornar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanca();
            }
        });
    }


    public void mostrar() {
        textView.setText(observacio);
        super.show();
    }

    public void tanca() {
        super.dismiss();
    }


}