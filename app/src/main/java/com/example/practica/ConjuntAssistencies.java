package com.example.practica;

import android.database.Cursor;
import android.util.Log;

import static com.example.practica.AppDbContract.*;

public class ConjuntAssistencies {

    private Cursor cursor;
    private AppDbManager appDbManager;
    private String assignatura;

    public ConjuntAssistencies(AppDbManager appDbManager, String dia, String assignatura) {
        this.appDbManager = appDbManager;
        this.assignatura = assignatura;
        this.cursor = appDbManager.getAssistenciesDia(dia, assignatura);
        Log.i("CJTASSISTENCIES", "La llista té num elements: " + Integer.toString(cursor.getCount()));
    }

    public ConjuntAssistencies(AppDbManager appDbManager, Estudiant estudiant, String assignatura) {
        this.appDbManager = appDbManager;
        this.assignatura = assignatura;
        this.cursor = appDbManager.getAssistenciesEstudiant(estudiant, assignatura);
    }

    /**
     * Creem les assistències per aquell dia
     *
     * @param dia
     */
    public void inicialitzaDia(String dia) {
        ConjuntEstudiants conjuntEstudiants = new ConjuntEstudiants(appDbManager, assignatura);
        Estudiant estudiant = conjuntEstudiants.getNextEstudiant();
        while (estudiant != null) {
            afegeixAssistencia(new Assistencia(estudiant, dia, assignatura));
            Log.i("CJTASSISTENCIES", "S'ha afegit una assistència per " + estudiant);
            estudiant = conjuntEstudiants.getNextEstudiant();
        }
    }

    public Assistencia getNextAssistencia() {
        // Retornem null si ja hem acabat
        if (cursor.isAfterLast())
            return null;
        String num_expedient = cursor.getString(cursor.getColumnIndex(Assistencies.COLUMNA_NUM_EXPEDIENT));
        String nom = cursor.getString(cursor.getColumnIndex(Estudiants.COLUMNA_NOM));
        int assistencia = cursor.getInt(cursor.getColumnIndex(Assistencies.COLUMNA_ASSISTENCIA));
        String observacio = cursor.getString(cursor.getColumnIndex(Assistencies.COLUMNA_OBSERVACIO));
        int index = cursor.getColumnIndex(Assistencies.COLUMNA_DIA);
        Log.i("CJTASSISTENCIES", Integer.toString(index));
        String dia = cursor.getString(index);
        dia = canviarFormatData(dia);
        cursor.moveToNext();
        return new Assistencia(new Estudiant(num_expedient, nom), dia, assistencia, observacio, assignatura);
    }

    /**
     * Afegeix l'estudiant donat al conjunt
     *
     * @param assistencia
     */
    public void afegeixAssistencia(Assistencia assistencia) {
        String num_expedient = assistencia.getEstudiant().getNum_expedient();
        int assis = assistencia.getAssistencia();
        String dia = assistencia.getDia();
        String observacio = assistencia.getObservacio();
        appDbManager.insertAssistencia(num_expedient, dia, assignatura, observacio, assis);
        cursor = appDbManager.getAssistenciesDia(dia, assignatura);
        Log.i("CJTASSISTENCIES", "S'ha creat una assistencia");
    }

    public void actualitzaAssistencia(Assistencia assistencia) {
        String num_expedient = assistencia.getEstudiant().getNum_expedient();
        int assis = assistencia.getAssistencia();
        String dia = assistencia.getDia();
        String observacio = assistencia.getObservacio();
        appDbManager.updateAssistencia(num_expedient, dia, assignatura, observacio, assis);
        cursor = appDbManager.getAssistenciesDia(dia, assignatura);
    }


    public String canviarFormatData(String data){
        String dataParts[] = data.split("/");

        String part0 = dataParts[0];
        String part1 = dataParts[1];
        String part2 = dataParts[2];

        if(part0.length() ==1){
            part0 = "0" + part0;
        }

        if(part1.length() ==1){
            part1 = "0" + part1;
        }

        if(part2.length() ==1){
            part2 = "0" + part2;
        }


        return part2 + "/" + part1 + "/" + part0;
    }

    /**
     * Retorna la mida del conjunt, el nombre d'estudiants
     *
     * @return
     */
    public int size() {
        if (cursor == null)
            return 0;
        return cursor.getCount();
    }


    public Assistencia getAssistencia(int index) {
        cursor.moveToFirst();
        cursor.move(index);
        Assistencia assistencia = getNextAssistencia();
        Log.i("CJTASSIST", assistencia.toString());
        return assistencia;
    }

    public String getAssignatura() {
        return assignatura;
    }
}
