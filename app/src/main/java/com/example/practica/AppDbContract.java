package com.example.practica;

import android.provider.BaseColumns;

public final class AppDbContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private AppDbContract() {
    }

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 25;
    public static final String DATABASE_NAME = "App.db";

    /* Inner class that defines the table contents */


    public static class Estudiants_Assignatures implements BaseColumns {

        public static final String NOM_TAULA = "estudiants_assignatures";
        public static final String COLUMNA_NUM_EXPEDIENT = "num_expedient";
        public static final String COLUMNA_ASSIGNATURA = "assignatura";

        public static final String CREATE_TABLE = "CREATE TABLE " + NOM_TAULA + "(" +
                COLUMNA_NUM_EXPEDIENT + " TEXT NOT NULL, " +
                COLUMNA_ASSIGNATURA + " TEXT NOT NULL, "   +
                " PRIMARY KEY (" + COLUMNA_NUM_EXPEDIENT + "," + COLUMNA_ASSIGNATURA + "))";


        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + NOM_TAULA;

    }


    public static class Assistencies implements BaseColumns {
        public static final String NOM_TAULA = "assistencies";
        public static final String COLUMNA_DIA = "dia";
        public static final String COLUMNA_NUM_EXPEDIENT = "num_expedient";
        public static final String COLUMNA_ASSIGNATURA = "assignatura";
        public static final String COLUMNA_OBSERVACIO = "observacio";
        public static final String COLUMNA_ASSISTENCIA = "assistencia";
        public static final String CREATE_TABLE = "CREATE TABLE " + NOM_TAULA + "(" +
                COLUMNA_DIA + " TEXT NOT NULL," +
                COLUMNA_NUM_EXPEDIENT + " TEXT NOT NULL," +
                COLUMNA_ASSIGNATURA + " TEXT NOT NULL," +
                COLUMNA_OBSERVACIO + " TEXT," +
                COLUMNA_ASSISTENCIA + " INTEGER NOT NULL," +
                " PRIMARY KEY (" + COLUMNA_DIA + "," + COLUMNA_NUM_EXPEDIENT + "," + COLUMNA_ASSIGNATURA + ")," +
                " FOREIGN KEY(" + COLUMNA_NUM_EXPEDIENT + ") REFERENCES " + Estudiants_Assignatures.NOM_TAULA + "(" + Estudiants_Assignatures.COLUMNA_NUM_EXPEDIENT + "))";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + NOM_TAULA;

    }


    public static class Estudiants implements BaseColumns {
        public static final String NOM_TAULA = "estudiants";
        public static final String COLUMNA_NUM_EXPEDIENT = "num_expedient";
        public static final String COLUMNA_NOM = "nom";

        public static final String CREATE_TABLE = "CREATE TABLE " + NOM_TAULA + "(" +
                COLUMNA_NUM_EXPEDIENT + " TEXT PRIMARY KEY NOT NULL, " +
                COLUMNA_NOM + " TEXT NOT NULL )";


        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + NOM_TAULA;


    }







}
