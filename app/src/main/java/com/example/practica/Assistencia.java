package com.example.practica;

public class Assistencia {
    private Estudiant estudiant;
    private String observacio;
    private int assistencia;
    private String dia;
    private String assignatura;

    public Assistencia(Estudiant estudiant, String dia, String assignatura) {
        this.estudiant = estudiant;
        this.observacio = "";
        this.assistencia = 0;
        this.dia = dia;
        this.assignatura = assignatura;
    }

    public Assistencia(Estudiant estudiant, String dia, int assistencia, String observacio, String assignatura) {
        this.estudiant = estudiant;
        this.dia = dia;
        this.observacio = observacio;
        this.assistencia = assistencia;
        this.assignatura = assignatura;
    }

    public Estudiant getEstudiant() {
        return estudiant;
    }

    public String getDia() {
        return dia;
    }

    public int getAssistencia() {
        return assistencia;
    }

    public void setAssistencia(int assistencia) {
        this.assistencia = assistencia;
    }

    public String getObservacio() {
        return observacio;
    }

    public void setObservacio(String observacio) {
        this.observacio = observacio;
    }

    @Override
    public String toString(){
        return estudiant.toString() + " " + assignatura + " " + assistencia + " " + observacio;
    }
}
