package com.example.practica;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

//ListView d'assistencies per un estudiant
public class AssistenciaEstudiantAdapter extends BaseAdapter {
    private Context context;
    private ConjuntAssistencies conjuntAssistencies;

    public AssistenciaEstudiantAdapter(Context context, ConjuntAssistencies conjuntAssistencies){
        this.context = context;
        this.conjuntAssistencies = conjuntAssistencies;
    }


    @Override
    public int getCount() {
        return conjuntAssistencies.size();
    }

    @Override
    public Object getItem(int position) {
        return conjuntAssistencies.getAssistencia(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        //Infla el layout de cada dia
        convertView = LayoutInflater.from(context).inflate(R.layout.estudiant_assistencia_dia, parent, false);

        final Assistencia assistencia = conjuntAssistencies.getAssistencia(position);
        TextView dia = convertView.findViewById(R.id.diaID);
        dia.setText(assistencia.getDia());

        final int assis = assistencia.getAssistencia();
        String assistenciaString = context.getResources().getStringArray(R.array.Opcions_spinner)[assis];
        TextView assistenciaText = convertView.findViewById(R.id.assistenciaID);
        assistenciaText.setText(assistenciaString);

        final ImageButton botoObs = convertView.findViewById(R.id.obsID);
        botoObs.setBackgroundColor(Color.rgb(80,120,200));
        if (assistencia.getObservacio().isEmpty()){
            botoObs.setVisibility(View.INVISIBLE);
        }
        final ObservacioLectura observacioLectura = new ObservacioLectura(context,assistencia.getObservacio());
        botoObs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                observacioLectura.mostrar();
            }
        });
        return convertView;
    }

}
