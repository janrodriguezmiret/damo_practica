package com.example.practica;

import android.database.Cursor;
import android.util.Log;

import static com.example.practica.AppDbContract.*;

public class ConjuntEstudiants {

    private Cursor cursor;
    private AppDbManager appDbManager;
    private String assignatura;

    public ConjuntEstudiants(AppDbManager appDbManager, String assignatura) {
        this.appDbManager = appDbManager;
        Log.i("23", "DEBUG23");
        this.cursor = appDbManager.getEstudiantsAssignatura(assignatura);
        Log.i("24", "DEBUG24");
        this.assignatura = assignatura;
        cursor.moveToFirst();
    }

    public Estudiant getNextEstudiant() {
        // Retornem null si ja hem acabat
        if (cursor.isAfterLast())
            return null;
        Log.i("DEBUG",cursor.getColumnNames()[0]);
        int num_index = cursor.getColumnIndex(Estudiants_Assignatures.COLUMNA_NUM_EXPEDIENT);
        Log.i("DEBUG",Integer.toString(num_index));
        String num_expedient = cursor.getString(0);
        String nom = cursor.getString(cursor.getColumnIndex(Estudiants.COLUMNA_NOM));
        cursor.moveToNext();
        return new Estudiant(num_expedient, nom);
    }

    /**
     * Afegeix l'estudiant donat al conjunt
     *
     * @param estudiant
     */
    /*public void afegeixEstudiant(Estudiant estudiant, String[] assignatures) {
        Log.i("CJTESTUDIANTS","S'ha creat un estudiant: "+estudiant);
        String num_expedient = estudiant.getNum_expedient();
        String nom = estudiant.getNom();
        appDbManager.insertEstudiant(num_expedient,nom);
    }*/


    /**
     * Retorna la mida del conjunt, el nombre d'estudiants
     *
     * @return
     */
    public int size() {
        if (cursor == null)
            return 0;
        return cursor.getCount();
    }

    /**
     * Retorna l'estudiant en la posició donada (index)
     *
     * @param index
     * @return
     */
    public Estudiant get(int index) {
        cursor.moveToFirst();
        cursor.move(index);
        return getNextEstudiant();
    }


}
