package com.example.practica;

import android.util.Log;

public class Estudiant {

    private String nom;
    private String num_expedient;


    public Estudiant(String num_expedient, String nom) {
        this.nom = nom;
        this.num_expedient = num_expedient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNum_expedient() {
        return num_expedient;
    }

    @Override
    public String toString() {
        return num_expedient + " - " + nom;
    }
}
