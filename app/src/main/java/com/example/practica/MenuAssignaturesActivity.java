package com.example.practica;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.List;

public class MenuAssignaturesActivity extends Activity {

    private AppDbManager appDbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assignatures);
        inicialitza();
    }

    private void inicialitza() {

        appDbManager = new AppDbManager(this);


        ImageButton buttonCatala =  findViewById(R.id.idCatala);

        buttonCatala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Català");
            }
        });
        ImageButton buttonCastella = findViewById(R.id.idCastella);
        buttonCastella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Castellà");
            }
        });
        ImageButton buttonAngles = findViewById(R.id.idAngles);
        buttonAngles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Anglès");
            }
        });
        ImageButton buttonMatematiques = findViewById(R.id.idMates);
        buttonMatematiques.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Matemàtiques");
            }
        });
        ImageButton buttonTecnologia = findViewById(R.id.idTecnologia);
        buttonTecnologia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Tecnologia");
            }
        });
        ImageButton buttonCienciesNaturals = findViewById(R.id.idCienciesNaturals);
        buttonCienciesNaturals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Ciències naturals");
            }
        });
        ImageButton buttonPlastica = findViewById(R.id.idPlastica);
        buttonPlastica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Plàstica");
            }
        });
        ImageButton buttonHistoria = findViewById(R.id.idHistoria);
        buttonHistoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPassarLlistaActivity("Història");
            }
        });


        ImageButton botoAfegirEstudiant = findViewById(R.id.botoafegirEstudiantDialog);
        botoAfegirEstudiant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDialogAfegirEstudiant();
            }
        });

    }


    public void afegeixEstudiant(Estudiant estudiant, List<String> assignatures) {
        String num_expedient = estudiant.getNum_expedient();
        String nom = estudiant.getNom();
        appDbManager.insertEstudiant(num_expedient, nom, assignatures);
    }

    private void mostrarDialogAfegirEstudiant() {
        AfegirEstudiantDialog estudiantdialog = new AfegirEstudiantDialog(this);
        estudiantdialog.mostrar();
    }

    private void mostrarDialogAfegirEstudiant(String assignatura) {
        AfegirEstudiantDialog estudiantdialog = new AfegirEstudiantDialog(this, assignatura);
        estudiantdialog.mostrar();
    }

    private void openPassarLlistaActivity(String assignatura) {
        ConjuntEstudiants conjuntEstudiants = new ConjuntEstudiants(appDbManager, assignatura);
        if (conjuntEstudiants.size() == 0) {
            mostrarDialogNoEstudiants(assignatura);
        } else {
            Intent intent = new Intent(this, PassarLlistaActivity.class);
            Bundle extras = new Bundle();
            extras.putString("assignatura", assignatura);
            intent.putExtras(extras);
            startActivity(intent);
        }
    }

    private void mostrarDialogNoEstudiants(final String assignatura) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.no_estudiants_message);
        builder.setTitle(R.string.no_estudiants_title);
        builder.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mostrarDialogAfegirEstudiant(assignatura);
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void openHelpActivity() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    public void openAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_help:
                openHelpActivity();
                return true;
            case R.id.action_about:
                openAboutActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





}
