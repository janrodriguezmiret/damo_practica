package com.example.practica;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class Observacio extends Dialog {

    private Assistencia assistencia;
    private EditText obs;
    // public int estat // si volem fer-ho amb estat

    public Observacio(final PassarLlistaActivity passarLlistaActivity, final Assistencia assistencia)
    {
        super(passarLlistaActivity);
        this.setContentView(R.layout.observacions);
        this.assistencia = assistencia;

        ImageButton botoAcceptar = findViewById(R.id.acceptarButton);
        ImageButton botoCancelar = findViewById(R.id.cancelarButton);

        botoAcceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (obs.getText().toString() != assistencia.getObservacio()) {
                    assistencia.setObservacio(obs.getText().toString());
                    passarLlistaActivity.actualitzaAssistencia(assistencia);
                }
                tanca();
            }
        });

        botoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanca();
            }
        });

    }


    public void mostrar(){

        obs =  findViewById(R.id.observacions);
        obs.setText(assistencia.getObservacio());
        super.show();
    }
    public void tanca(){
        super.dismiss();
    }



}
